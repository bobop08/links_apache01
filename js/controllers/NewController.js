app.controller("News", function($scope) {
    $scope.tag = "News";
    $scope.NewsURLS = [
        {
            name: "CNN",
            url: "https://www.cnn.com"
        },
        {
            name: "BBC",
            url: "https://www.bbc.com"
        },
        {
            name: "GMA",
            url: "https://www.gma.com"
        },
        {
            name: "ABS",
            url: "https://www.abs.com"
        }
    ]

});