var app = angular.module("myApp", ['ngRoute','ngResource']);

app.config(function($routeProvider) {
    $routeProvider
    .when('/', {
      templateUrl : 'pages/main.html'
    })
    .when('/news', {
      templateUrl : 'pages/news.html',
    })
    .when('/social', {
      templateUrl : 'pages/social.html'
    })
    .when('/main', {
      templateUrl : 'pages/main.html'
    });
  });
  